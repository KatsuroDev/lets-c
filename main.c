#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void Miaow() {
  printf("Miaow!\n");
}

void Woof() {
  printf("Woof!\n");
}

typedef struct {
  const char* name; //string.
  bool gender_is_male;   // true or false.
  unsigned int age;      // number.
  void (*speak)();
} pet;

void print_pet(pet pet_to_print) {

  printf("Pet '%s' is %d years old and it's a %s.\n",
          pet_to_print.name, pet_to_print.age, (pet_to_print.gender_is_male ? "male" : "female"));
  pet_to_print.speak();

}

int main()
{
    pet ezras_pet = {"Joeblow", false, 10, Woof};

    pet katsuros_pet;
    katsuros_pet.name = "Link";
    katsuros_pet.gender_is_male = true;
    katsuros_pet.age = 1;
    katsuros_pet.speak = Miaow;
    printf("%p\n", katsuros_pet);

    print_pet(ezras_pet);
    print_pet(katsuros_pet);

    return 0;
}
